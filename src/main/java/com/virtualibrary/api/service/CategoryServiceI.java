package com.virtualibrary.api.service;

import java.util.List;
import com.virtualibrary.api.model.Category;

public interface CategoryServiceI {
    public Category insert (Category category);

    public Category getById(int id);

    public List<Category> getAll();

    public Category update(Category category);

    public void delete(int id);
}
