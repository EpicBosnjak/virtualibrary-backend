package com.virtualibrary.api.service;

import com.virtualibrary.api.dto.insert.InsertBookDTO;
import com.virtualibrary.api.dto.update.UpdateBookDTO;
import com.virtualibrary.api.enums.Status;
import com.virtualibrary.api.exception.ConflictException;
import com.virtualibrary.api.exception.NotFoundException;
import com.virtualibrary.api.model.*;
import com.virtualibrary.api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;


@Service
public class BookServiceImpl implements BookServiceI {

    public BookRepo bookRepo;
    public CategoryRepo categoryRepo;
    public AuthorRepo authorRepo;
    public PublisherRepo publisherRepo;
    public FileRepo fileRepo;
    public Environment environment;


    @Transactional
    @Override
    @CrossOrigin
    public Book insert(InsertBookDTO bookDTO) {
        Book insertBook = mapToBook(bookDTO);
        bookRepo.save(insertBook);
        return insertBook;
    }

    @Override
    public List<Book> getAll() {
        return bookRepo.findAllByStatus(Status.ACTIVE.getValue());
    }

    @Override
    public Book getById(int id) {
        Book book = bookRepo.findById(id);
        if(book == null)
            throw new NotFoundException(Book.class.getSimpleName(),id);

        return book;
    }

    @Override
    public Book update(UpdateBookDTO book) {
        Book toUpdate = bookRepo.findById(book.getId());
        if(toUpdate == null)
            throw new NotFoundException(Book.class.getSimpleName(),book.getId());
        toUpdate.setName(book.getName());
        toUpdate.setPublishedYear(book.getPublishedYear());
        //toUpdate.setDescription(book.getDesription());

        return bookRepo.save(toUpdate);
    }


    @Transactional
     Book mapToBook(InsertBookDTO bookDTO){
        Book uniqueISBN = bookRepo.findByIsbn(bookDTO.getIsbn());
        if(uniqueISBN!= null)
            throw new ConflictException();

        Book book = new Book();
        book.setName(bookDTO.getName());
        book.setDescription(bookDTO.getDescription());
        book.setIsbn(bookDTO.getIsbn());
        book.setPages(0);
        book.setPublishedYear(bookDTO.getPublishedYear());
        book.setStatus(Status.PADDING.getValue());

        List<Category> categories = new ArrayList<>();
        for(Integer id : bookDTO.getCategories()){
            categories.add(categoryRepo.findById(id.intValue()));
        }
        if(!categories.isEmpty())
            book.setCategories(categories);

        List<Author> authors = new ArrayList<>();
        for(Integer id : bookDTO.getAuthors()){
            authors.add(authorRepo.findById(id.intValue()));
        }
        if(!authors.isEmpty())
            book.setAuthors(authors);

        if(bookDTO.getPublisher()!= null) {
            Publisher publisher = publisherRepo.findById(bookDTO.getPublisher().intValue());
            if (publisher != null)
                book.setPublisher(publisher);
        }

        String fileName =bookDTO.getName() + new SimpleDateFormat("-dd-MM-yyyy-HH-mm-ss").format(new Date());

        String path = getProperty("file.path.uploads") + fileName + "/";
        File dir = new File(path);
        if(dir.mkdir()){

            FileEntity fileEntity = new FileEntity();
            fileEntity.setName(fileName);
            String encodePath = Base64.getEncoder().encodeToString(path.getBytes());

            fileEntity.setPath(encodePath);
            FileEntity insertFile = fileRepo.save(fileEntity);
            book.setFileEntity(insertFile);
        }

        Book insertBook = bookRepo.save(book);

        return insertBook;

    }





    private String getProperty(String property){
        return environment.getProperty(property);
    }


    @Autowired
    public void setBookRepo(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }
    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
    @Autowired
    public void setCategoryRepo(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }
    @Autowired
    public void setAuthorRepo(AuthorRepo authorRepo) {
        this.authorRepo = authorRepo;
    }
    @Autowired
    public void setPublisherRepo(PublisherRepo publisherRepo) {
        this.publisherRepo = publisherRepo;
    }
    @Autowired
    public void setFileRepo(FileRepo fileRepo) {
        this.fileRepo = fileRepo;
    }
}
