package com.virtualibrary.api.service;

import com.virtualibrary.api.model.Role;

public interface RoleServiceI {
    Role findByName(String user);
}
