package com.virtualibrary.api.service;

import com.virtualibrary.api.exception.NotFoundException;
import com.virtualibrary.api.model.ConfirmationToken;
import com.virtualibrary.api.repository.ConfirmationTokenRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfirmationTokenServiceImpl implements ConfirmationTokenServiceI {
    @Autowired
    ConfirmationTokenRepo tokenRepo;

    @Override
    public ConfirmationToken insert(ConfirmationToken token) {
        System.out.println("TOKEN "+token.getToken());
        return tokenRepo.save(token);
    }

    @Override
    public ConfirmationToken findByConfirmationToken(String token) {
        ConfirmationToken confirmationToken = tokenRepo.findByToken(token);
        if(confirmationToken == null)
            throw new NotFoundException("Token is not found");
        return confirmationToken;
    }


}
