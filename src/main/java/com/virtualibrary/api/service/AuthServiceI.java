package com.virtualibrary.api.service;

import com.virtualibrary.api.dto.LoginDTO;
import com.virtualibrary.api.dto.TokenDTO;
import com.virtualibrary.api.dto.UserDTO;
import com.virtualibrary.api.dto.insert.InsertUserDTO;
import com.virtualibrary.api.model.User;


public interface AuthServiceI {

    UserDTO login(LoginDTO loginDTO);

    User registerUser(InsertUserDTO user);

    User confirmUserAccount(String token);
}
