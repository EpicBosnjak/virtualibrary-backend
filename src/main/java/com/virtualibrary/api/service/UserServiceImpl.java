package com.virtualibrary.api.service;

import com.virtualibrary.api.dto.update.BookMarkDTO;
import com.virtualibrary.api.enums.Status;
import com.virtualibrary.api.exception.ConflictException;
import com.virtualibrary.api.exception.NotFoundException;
import com.virtualibrary.api.model.User;
import com.virtualibrary.api.repository.BookCopyRepo;
import com.virtualibrary.api.repository.RoleRepo;
import com.virtualibrary.api.repository.UserRepo;
import com.virtualibrary.api.security.TokenUtils;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import springfox.documentation.spring.web.json.Json;

@Service
public class UserServiceImpl implements UserServiceI {
    private UserRepo userRepo;
    private BookCopyRepo bookCopyRepo;
    private RoleRepo roleRepo;

    private AuthenticationManager authenticationManager;
    private TokenUtils tokenUtils;
    private UserDetailsService userDetailsService;





    @Override
    public User getById(int id) {
        User user = userRepo.findByIdAndStatus(id, Status.ACTIVE.getValue());
        if(user == null)
            throw new NotFoundException(User.class.getSimpleName(),id);
        return user;
    }



    @Override
    public User findByEmail(String email) {
        User user = userRepo.findByEmailAndStatus(email, Status.ACTIVE.getValue());
        if(user==null){
            throw new NotFoundException("User with mail " + email + " does not exist.");
        }
        return user;
    }


    @Override
    public User insert(User insertUser) {
        User verificationUser = userRepo.findByEmail(insertUser.getEmail());
        if(verificationUser!=null){
            throw new ConflictException();
        }
        return userRepo.save(insertUser);
    }


    @Override
    public User findByIdAndStatus(int id, int value) {
        User user = userRepo.findByIdAndStatus(id,value);
        if(user==null){
            throw new NotFoundException(User.class.getSimpleName(), id);
        }
        return user;
    }

    @Override
    public boolean doesMailExist(String email) {
        User user = userRepo.findByEmail(email);
        return !(user==null);
    }

    @Override
    public JSONObject updateBookMark(BookMarkDTO bookMarkDTO) {
        User user = userRepo.findByIdAndStatus(bookMarkDTO.getUserId(),Status.ACTIVE.getValue());
        if(user == null)
            throw new NotFoundException(User.class.getSimpleName(),bookMarkDTO.getUserId());

        user.setBookmark(user.getBookmark()+bookMarkDTO.getBookMark());
        User data = userRepo.save(user);

        JSONObject response = new JSONObject();
        response.put("bookMark",data.getBookmark());
        return response;
    }


    @Autowired
    public void setRoleRepo(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setBookCopyRepo(BookCopyRepo bookCopyRepo) {
        this.bookCopyRepo = bookCopyRepo;
    }





}
