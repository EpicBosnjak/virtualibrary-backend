package com.virtualibrary.api.service;

import com.virtualibrary.api.dto.insert.InsertBookCopyDTO;
import com.virtualibrary.api.enums.Status;
import com.virtualibrary.api.exception.BadRequestException;
import com.virtualibrary.api.exception.NotFoundException;
import com.virtualibrary.api.model.Book;
import com.virtualibrary.api.model.BookCopy;
import com.virtualibrary.api.model.User;
import com.virtualibrary.api.repository.BookCopyRepo;
import com.virtualibrary.api.repository.BookRepo;
import com.virtualibrary.api.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Date;
import java.util.List;

@Service
public class BookCopyServiceImpl implements BookCopyServiceI {

    private BookCopyRepo bookCopyRepo;
    private BookRepo bookRepo;
    private UserRepo userRepo;

    @Override
    public BookCopy insert(InsertBookCopyDTO bookCopyDTO) {
        Book book = bookRepo.findById(bookCopyDTO.getBookId());
        if(book == null) {
            throw new NotFoundException(Book.class.getSimpleName(), bookCopyDTO.getBookId());
        }
        User user = userRepo.findByIdAndStatus(bookCopyDTO.getUserId(), Status.ACTIVE.getValue());
        if(user == null)
            throw new NotFoundException(User.class.getSimpleName(),bookCopyDTO.getUserId());

        if(user.getBookmark()<1)
            throw new BadRequestException("Sorry, not enough bookmarks ");

        BookCopy bookCopy = new BookCopy();
        bookCopy.setCurrentPage(1);
        bookCopy.setRentDate(new Date());
        bookCopy.setExpiryDate(new Date());
        bookCopy.setUser(user);
        bookCopy.setBook(book);

        BookCopy newBookCopy = bookCopyRepo.save(bookCopy);

        user.setBookmark(user.getBookmark()-1);
        userRepo.save(user);

        return newBookCopy;
    }

    @Override
    public List<BookCopy> findByUserId(int userId) {
        User user = userRepo.findByIdAndStatus(userId, Status.ACTIVE.getValue());
        if(user == null)
            throw new NotFoundException(User.class.getSimpleName(),userId);
        List<BookCopy> bookCopies = bookCopyRepo.findAllByUser(user);
        return bookCopies;

    }

    @Override
    public List<BookCopy> findByBookId(int bookId) {
        Book book = bookRepo.findById(bookId);
        if(book == null)
            throw new NotFoundException(Book.class.getSimpleName(),bookId);
        List<BookCopy> bookCopies = bookCopyRepo.findAllByBook(book);
        return bookCopies;
    }

    @Override
    public BookCopy update(BookCopy bookCopy) {
        BookCopy toUpdateBookCopy = bookCopyRepo.findById(bookCopy.getId());
        toUpdateBookCopy.setCurrentPage(bookCopy.getCurrentPage());
        return bookCopyRepo.save(toUpdateBookCopy);
    }

    @Override
    public BookCopy findById(int id) {
        BookCopy bookCopy = bookCopyRepo.findById(id);
        if(bookCopy==null)
            throw new NotFoundException(BookCopy.class.getSimpleName(),id);
        return bookCopy;
    }

    @Override
    public BookCopy currentPage(Integer id, Integer page) {
        BookCopy bookCopy = bookCopyRepo.findById(id.intValue());
        if(bookCopy==null)
            throw new NotFoundException(BookCopy.class.getSimpleName(),id);

        if(page > 0 && page <= bookCopy.getBook().getPages()){
            bookCopy.setCurrentPage(page);
            return bookCopyRepo.save(bookCopy);
        }else{
            throw new BadRequestException("There is no " + page +". page in this book");
        }

    }

    @Autowired
    public void setBookCopyRepo(BookCopyRepo bookCopyRepo) {
        this.bookCopyRepo = bookCopyRepo;
    }

    @Autowired
    public void setBookRepo(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }


}
