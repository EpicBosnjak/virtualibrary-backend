package com.virtualibrary.api.service;

import com.virtualibrary.api.dto.insert.InsertBookCopyDTO;
import com.virtualibrary.api.model.BookCopy;

import java.util.List;

public interface BookCopyServiceI {
    BookCopy insert(InsertBookCopyDTO bookCopyDTO);

    List<BookCopy> findByUserId(int intValue);

    List<BookCopy> findByBookId(int intValue);

    BookCopy update(BookCopy bookCopy);

    BookCopy findById(int intValue);

    BookCopy currentPage(Integer id, Integer page);
}
