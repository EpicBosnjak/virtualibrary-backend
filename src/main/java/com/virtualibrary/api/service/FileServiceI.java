package com.virtualibrary.api.service;

import com.virtualibrary.api.model.FileEntity;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

public interface FileServiceI {

    public void fileUpload(MultipartFile file, int bookId);
    public String getFilePath(String fileName);
    public Resource getPage(String path, int page);

    Resource getImage(String path);
}
