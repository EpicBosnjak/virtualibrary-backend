package com.virtualibrary.api.service;

import com.virtualibrary.api.exception.ConflictException;
import com.virtualibrary.api.exception.NotFoundException;
import com.virtualibrary.api.model.Publisher;
import com.virtualibrary.api.repository.BookRepo;
import com.virtualibrary.api.repository.PublisherRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PublisherServiceImpl implements PublisherServiceI {

    private PublisherRepo publisherRepo;
    private BookRepo bookRepo;


    @Override
    public Publisher insert(Publisher publisher) {
        Publisher result = publisherRepo.findByNameIgnoreCase(publisher.getName());
        if(result != null)
            throw new ConflictException();

        return publisherRepo.save(publisher);
    }

    @Override
    public Publisher getById(int id) {
        Publisher result = publisherRepo.findById(id);
        if (result == null)
            throw new NotFoundException(Publisher.class.getSimpleName(),id);
        return result;
    }

    @Override
    public List<Publisher> getAll() {
        return publisherRepo.findAll();
    }

    @Override
    public Publisher update(Publisher publisher) {
        Publisher toUpdate = publisherRepo.findById(publisher.getId());
        if (toUpdate==null)
            throw new NotFoundException(Publisher.class.getSimpleName(),publisher.getId());
        toUpdate.setName(publisher.getName());
        //toUpdate.setBooks(publisher.getBooks());
        return publisherRepo.save(toUpdate);
    }

    @Override
    public void delete(int id) {
        Publisher toDelete = publisherRepo.findById(id);
        if (toDelete==null)
            throw new NotFoundException(Publisher.class.getSimpleName(),id);
        publisherRepo.delete(toDelete);
    }


    @Autowired
    public void setPublisherRepo(PublisherRepo publisherRepo) {
        this.publisherRepo = publisherRepo;
    }

    @Autowired
    public void setBookRepo(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }
}
