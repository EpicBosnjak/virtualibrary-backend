package com.virtualibrary.api.service;

import com.virtualibrary.api.dto.insert.InsertBookDTO;
import com.virtualibrary.api.dto.update.UpdateBookDTO;
import com.virtualibrary.api.model.Book;

import java.util.List;

public interface BookServiceI {
    Book insert(InsertBookDTO bookDTO);

    List<Book> getAll();

    Book getById(int id);

    Book update(UpdateBookDTO book);
}
