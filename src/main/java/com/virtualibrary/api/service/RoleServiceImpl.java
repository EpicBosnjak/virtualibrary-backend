package com.virtualibrary.api.service;

import com.virtualibrary.api.model.Role;
import com.virtualibrary.api.repository.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleServiceI {

    RoleRepo roleRepo;

    @Override
    public Role findByName(String role) {
        return roleRepo.findByName(role);
    }

    @Autowired
    public void setRoleRepo(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }
}
