package com.virtualibrary.api.service;

import com.virtualibrary.api.exception.ConflictException;
import com.virtualibrary.api.exception.NotFoundException;
import com.virtualibrary.api.model.Category;
import com.virtualibrary.api.repository.BookRepo;
import com.virtualibrary.api.repository.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryServiceI{
    private CategoryRepo categoryRepo;
    private BookRepo bookRepo;


    @Override
    public Category insert(Category category) {
        Category result = categoryRepo.findByName(category.getName());
        if (result != null)
            throw new ConflictException();
        return categoryRepo.save(category);
    }

    @Override
    public Category getById(int id) {
        Category category = categoryRepo.findById(id);
        if (category == null)
            throw new NotFoundException(Category.class.getSimpleName(),id);
        return category;
    }

    @Override
    public List<Category> getAll() {
        return categoryRepo.findAll();
    }

    @Override
    public Category update(Category category) {
        Category toUpdate = categoryRepo.findById(category.getId());
        if (toUpdate==null)
            throw new NotFoundException(Category.class.getSimpleName(),category.getId());
        toUpdate.setName(category.getName());
        //toUpdate.setBooks(category.getBooks());
        return categoryRepo.save(toUpdate);
    }

    @Override
    public void delete(int id) {
        Category toDelete = categoryRepo.findById(id);
        if (toDelete == null)
            throw new NotFoundException(Category.class.getSimpleName(),id);

        categoryRepo.delete(toDelete);
    }

    @Autowired
    public void setCategoryRepo(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    @Autowired
    public void setBookRepo(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }
}
