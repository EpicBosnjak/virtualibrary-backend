package com.virtualibrary.api.service;

import com.virtualibrary.api.model.ConfirmationToken;

public interface ConfirmationTokenServiceI {
    ConfirmationToken insert(ConfirmationToken token);

    ConfirmationToken findByConfirmationToken(String token);
}
