package com.virtualibrary.api.service;

import com.virtualibrary.api.exception.ConflictException;
import com.virtualibrary.api.exception.NotFoundException;
import com.virtualibrary.api.model.Author;
import com.virtualibrary.api.model.Book;
import com.virtualibrary.api.repository.AuthorRepo;
import com.virtualibrary.api.repository.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorServiceI {

    private AuthorRepo authorRepo;

    @Autowired
    public void setAuthorRepo(AuthorRepo authorRepo) {
        this.authorRepo = authorRepo;
    }

    @Override
    public Author insert(Author author) {
        Author tempAuthor = authorRepo.findByFirstNameIgnoreCaseAndLastNameIgnoreCase(author.getFirstName(),author.getLastName());
        if(tempAuthor != null)
            throw new ConflictException();
        return authorRepo.save(author);
    }

    @Override
    public Author getById(int id) {

         Author author = authorRepo.findById(id);
         if(author == null)
             throw  new NotFoundException(Author.class.getSimpleName(),id);

         return author;
    }

    @Override
    public List<Author> getAll() {
        return authorRepo.findAll();
    }


    @Override
    public Author update(Author author) {
        Author toUpdate = authorRepo.findById(author.getId());
        if (toUpdate==null)
            throw new NotFoundException(Author.class.getSimpleName(),author.getId());
        toUpdate.setFirstName(author.getFirstName());
        toUpdate.setLastName(author.getLastName());
        //toUpdate.setBooks(author.getBooks());
        return authorRepo.save(toUpdate);

    }

    @Override
    public void delete(int id) {
        Author toDelete = authorRepo.findById(id);
        if(toDelete==null)
            throw new NotFoundException(Author.class.getSimpleName(),id);
        authorRepo.delete(toDelete);
    }






}
