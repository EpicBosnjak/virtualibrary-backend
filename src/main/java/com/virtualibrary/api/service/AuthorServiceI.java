package com.virtualibrary.api.service;

import com.virtualibrary.api.model.Author;
import com.virtualibrary.api.model.Book;

import java.util.List;

public interface AuthorServiceI {
    public Author insert (Author author);

    public Author getById(int id);

    public List<Author> getAll();

    public Author update(Author author);

    public void delete(int id);

}
