package com.virtualibrary.api.service;

import com.virtualibrary.api.model.Publisher;

import java.util.List;

public interface PublisherServiceI {
    Publisher insert(Publisher publisher);

    Publisher getById(int id);

    List<Publisher> getAll();

    Publisher update(Publisher publisher);

    void delete(int id);
}
