package com.virtualibrary.api.service;

import com.virtualibrary.api.dto.LoginDTO;
import com.virtualibrary.api.dto.UserDTO;
import com.virtualibrary.api.dto.insert.InsertUserDTO;
import com.virtualibrary.api.dto.mapper.UserMapper;
import com.virtualibrary.api.enums.Status;
import com.virtualibrary.api.exception.BadRequestException;
import com.virtualibrary.api.exception.BadCredentialsException;
import com.virtualibrary.api.model.ConfirmationToken;
import com.virtualibrary.api.model.Role;
import com.virtualibrary.api.model.User;
import com.virtualibrary.api.repository.ConfirmationTokenRepo;
import com.virtualibrary.api.repository.UserRepo;
import com.virtualibrary.api.security.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.mail.SimpleMailMessage;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Random;

@Service
public class AuthServiceImpl implements AuthServiceI {

    private AuthenticationManager authenticationManager;
    private TokenUtils tokenUtils;
    private UserDetailsService userDetailsService;
    private UserServiceI userService;
    private RoleServiceI roleService;
    private ConfirmationTokenServiceI confirmationTokenService;
    private EmailSenderService emailSenderService;
    private Environment environment;

    private UserRepo userRepo;
    private ConfirmationTokenRepo confirmationTokenRepo;

    @Override
    public UserDTO login(LoginDTO loginDTO) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    loginDTO.getEmail(), loginDTO.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getEmail());

            UserDTO userDTO = UserMapper.crateUserDTO(userRepo.findByEmail(loginDTO.getEmail()));
            userDTO.setToken(tokenUtils.generateToken(details));
            return userDTO;
        } catch (Exception e) {
            throw new BadCredentialsException();

        }
    }

    @Override
    public User registerUser(InsertUserDTO user) {

        if (userService.doesMailExist(user.getEmail())) {
            throw new BadRequestException("An account already exist with that login email.");
        }
        Role userRole = roleService.findByName("USER");
        User insertUser = new User();
        insertUser.setFirstName(user.getFirstName());
        insertUser.setLastName(user.getLastName());
        insertUser.setEmail(user.getEmail());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        insertUser.setPassword(encoder.encode(user.getPassword()));
        insertUser.setStatus(Status.PADDING.getValue());
        insertUser.setRole(userRole);


        User newUser = userService.insert(insertUser);
        System.out.println("safdas");
        ConfirmationToken token = new ConfirmationToken();
        token.setCreatedDate(new Date());
        token.setUser(newUser);
        token.setToken(generateToken());
        ConfirmationToken newToken = confirmationTokenService.insert(token);

        sendConfirmationMail(newUser,newToken.getToken());
        return newUser;

    }


    private void sendConfirmationMail(User user,String token) {


        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject(this.sendConfirmationMailTitle());
        StringBuilder messageBody = new StringBuilder();
        messageBody.append(this.sendConfirmationMailBody() +"\n");
        messageBody.append(this.loginPage() + "?token=" + token +" \n");
        messageBody.append(this.mailMessageFooter());

        mailMessage.setText(messageBody.toString());

        emailSenderService.sendEmail(mailMessage);
    }

   @Override
    public User confirmUserAccount(String token) {
        ConfirmationToken confirmationtoken = confirmationTokenService.findByConfirmationToken(token);
        if (confirmationtoken != null) {
            User user = userService.findByIdAndStatus(confirmationtoken.getUser().getId(), Status.PADDING.getValue());
            user.setStatus(Status.ACTIVE.getValue());
            userRepo.save(user);
            confirmationtoken.setConfirmedDate(new Date());
            confirmationTokenRepo.save(confirmationtoken);
            return user;
        }
        return null;
    }

    private String sendConfirmationMailTitle() {
        return environment.getProperty("authentication.mailMessage.sendConfirmationMail.title");
    }
    private String sendConfirmationMailBody() {
        return environment.getProperty("authentication.mailMessage.sendConfirmationMail.body");
    }
    private String loginPage() {
        //TODO: change to url.frontend
        return environment.getProperty("url.frontend")+"login" ;
    }
    private String mailMessageFooter() {
        return environment.getProperty("authentication.mailMessage.sendConfirmationMail.footer");
    }






    private String generateToken() {
        int n = 30;
        byte[] array = new byte[256];
        new Random().nextBytes(array);

        String randomString = new String(array, StandardCharsets.UTF_8);

        StringBuilder r = new StringBuilder();

        String AlphaNumericString = randomString.replaceAll("[^A-Za-z0-9]", "");

        for (int k = 0; k < AlphaNumericString.length(); k++) {
            if (Character.isLetter(AlphaNumericString.charAt(k)) && (n > 0) || Character.isDigit(AlphaNumericString.charAt(k)) && (n > 0)) {
                r.append(AlphaNumericString.charAt(k));
                n--;
            }
        }
        return r.toString();
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }

    @Autowired
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Autowired
    public void setUserService(UserServiceI userService) {
        this.userService = userService;
    }

    @Autowired
    public void setRoleService(RoleServiceI roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setTokenService(ConfirmationTokenServiceI tokenService) {
        this.confirmationTokenService = tokenService;
    }

    @Autowired
    public void setConfirmationTokenService(ConfirmationTokenServiceI confirmationTokenService) {
        this.confirmationTokenService = confirmationTokenService;
    }

    @Autowired
    public void setEmailSenderService(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }
    @Autowired
    public void setConfirmationTokenRepo(ConfirmationTokenRepo confirmationTokenRepo) {
        this.confirmationTokenRepo = confirmationTokenRepo;
    }
}
