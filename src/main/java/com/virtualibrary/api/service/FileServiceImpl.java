package com.virtualibrary.api.service;

import com.virtualibrary.api.enums.Status;
import com.virtualibrary.api.exception.ConflictException;
import com.virtualibrary.api.exception.NotFoundException;
import com.virtualibrary.api.model.Book;
import com.virtualibrary.api.model.FileEntity;
import com.virtualibrary.api.repository.BookRepo;
import com.virtualibrary.api.repository.FileRepo;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FilenameUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.concurrent.CompletableFuture;


@Service
public class FileServiceImpl implements FileServiceI {

    private FileRepo fileRepo;
    private BookRepo bookRepo;


    private Environment environment;

    @Override
    public void fileUpload(MultipartFile file,int bookId) {
        Book book = bookRepo.findById(bookId);
        if(book == null)
            throw new NotFoundException(Book.class.getSimpleName(),bookId);
        byte[] decodedBytes = Base64.getDecoder().decode(book.getFileEntity().getPath());
        String decodePath = new String(decodedBytes);
        Path filepath = Paths.get( decodePath + book.getName() + ".pdf");

        try (OutputStream os = Files.newOutputStream(filepath )) {
            os.write(file.getBytes());
            CompletableFuture.runAsync(()->{
                try {
                    this.pdfToImage(book.getFileEntity());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void pdfToImage (FileEntity toUpdate) throws  IOException{
        byte[] decodedBytes = Base64.getDecoder().decode(toUpdate.getPath());
        String decodePath = new String(decodedBytes);
        String newPath = decodePath + "bookContent/";
        File dir = new File(newPath.trim());
        dir.mkdir();
        final PDDocument document = PDDocument.load(new File(decodePath + "/" + toUpdate.getBook().getName() + ".pdf"));
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        for (int page = 0; page < document.getNumberOfPages(); ++page)
        {
            String fName = dir.getPath()+ "/"+ (page+1) + ".png";
            BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
            createWatermark(bim);
            ImageIOUtil.writeImage(bim, fName, 300);
        }

        String encodePath = Base64.getEncoder().encodeToString(newPath.trim().getBytes());
        String encodeImage = Base64.getEncoder().encodeToString((newPath+"/1.png").trim().getBytes());
        toUpdate.setPath(encodePath);

        fileRepo.save(toUpdate);

        Book bookToUpdate = toUpdate.getBook();
        bookToUpdate.setPages(document.getNumberOfPages());
        bookToUpdate.setStatus(Status.ACTIVE.getValue());
        bookToUpdate.setImage(encodeImage);

        bookRepo.save(bookToUpdate);

        document.close();
        System.out.println("Finished");
    }

    private void createWatermark(BufferedImage bim){
        Graphics graphics = bim.createGraphics();
        graphics.drawImage(bim, 0, 0, null);
        graphics.setFont(new Font("Arial", Font.PLAIN, 280));
        graphics.setColor(new Color(255, 0, 0, 40));
        String watermark = "VirtuaLibrary";
        graphics.drawString(watermark, bim.getWidth()/5,
                bim.getHeight()/2);
        graphics.dispose();

    }

    @Override
    public String getFilePath(String fileName) {
        return null;
    }

    @Override
    public Resource getPage(String path, int page) {
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(path);
            String decodePath = new String(decodedBytes);
            InputStreamResource resource = new InputStreamResource(new FileInputStream(decodePath +"/"+ page + ".png"));
            return resource;
        }catch (FileNotFoundException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Resource getImage(String path) {
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(path);
            String decodePath = new String(decodedBytes);
            InputStreamResource resource = new InputStreamResource(new FileInputStream(decodePath));
            return resource;
        }catch (FileNotFoundException e){
            e.printStackTrace();
            return null;
        }
    }

    private String getFilePath(){
        return environment.getProperty("file.path.uploads");
    }

    private FileEntity insert(FileEntity fileEntity){
        FileEntity result = fileRepo.findByNameIgnoreCaseAndPath(fileEntity.getName(), fileEntity.getPath());
        if(result!=null){
            throw new ConflictException();
        }
        return fileRepo.save(fileEntity);
    }



    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Autowired
    public void setFileRepo(FileRepo fileRepo) {
        this.fileRepo = fileRepo;
    }

    @Autowired
    public void setBookRepo(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }
}
