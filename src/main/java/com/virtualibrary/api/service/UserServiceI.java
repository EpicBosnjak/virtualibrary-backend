package com.virtualibrary.api.service;

import com.virtualibrary.api.dto.update.BookMarkDTO;
import com.virtualibrary.api.model.User;
import net.minidev.json.JSONObject;
import springfox.documentation.spring.web.json.Json;

public interface UserServiceI {
    User getById(int intValue);

    User findByEmail(String email);

    User insert(User insertUser);


    User findByIdAndStatus(int id, int value);

    boolean doesMailExist(String email);

    JSONObject updateBookMark(BookMarkDTO bookMarkDTO);
}
