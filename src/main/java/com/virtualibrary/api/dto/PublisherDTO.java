package com.virtualibrary.api.dto;

import com.virtualibrary.api.dto.simple.SimpleBookDTO;
import com.virtualibrary.api.dto.simple.SimplePublisherDTO;

import java.util.List;

public class PublisherDTO extends SimplePublisherDTO {
    private List<SimpleBookDTO> books;

    public List<SimpleBookDTO> getBooks() {
        return books;
    }

    public void setBooks(List<SimpleBookDTO> books) {
        this.books = books;
    }

    public void addToBooks(SimpleBookDTO simpleBookDTO){
        this.books.add(simpleBookDTO);
    }
}
