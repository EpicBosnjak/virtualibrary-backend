package com.virtualibrary.api.dto.simple;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleBookDTO {
    private int id;
    private String name;

    private Map<String, List<Map<String,String>> > links = new HashMap<>();

    public int getId() {
        return id;
    }

    public void setBookId(int bookId) {
        this.id = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addToHref(String key, List<Map<String,String>> value) {
        links.put(key,value);
    }

    public void setId(int id) {
        this.id = id;
    }


    public Map<String, List<Map<String, String>>> getLinks() {
        return links;
    }

    public void setLinks(Map<String, List<Map<String, String>>> links) {
        this.links = links;
    }

}
