package com.virtualibrary.api.dto.simple;

import java.util.HashMap;
import java.util.Map;

public class SimpleBookCopyDTO {

    private int id;
    private Map<String, Map<String, String>> links = new HashMap<>();
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Map<String, Map<String, String>> getLinks() {
        return links;
    }
    public void setLinks(Map<String, Map<String, String>> links) {
        this.links = links;
    }

    public void addToLink(String key,Map<String, String>value){
        this.links.put(key,value);
    }
}
