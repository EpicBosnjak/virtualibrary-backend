package com.virtualibrary.api.dto.simple;

import java.util.HashMap;
import java.util.Map;

public class SimpleAuthorDTO {
    private int id;
    private String firstName;
    private String lastName;
    private Map<String, Map<String, String>> links = new HashMap<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Map<String, Map<String, String>> getLinks() {
        return links;
    }

    public void setLinks(Map<String, Map<String, String>> links) {
        this.links = links;
    }

    public void addToLinks(String key,Map<String, String> values){
        this.links.put(key,values);
    }
}
