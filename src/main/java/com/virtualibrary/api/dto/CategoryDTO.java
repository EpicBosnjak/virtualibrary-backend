package com.virtualibrary.api.dto;


import com.virtualibrary.api.dto.simple.SimpleBookDTO;
import com.virtualibrary.api.dto.simple.SimpleCategoryDTO;

import java.util.ArrayList;
import java.util.List;

public class CategoryDTO extends SimpleCategoryDTO {
    public List<SimpleBookDTO> books = new ArrayList<>();

    public List<SimpleBookDTO> getBooks() {
        return books;
    }

    public void setBooks(List<SimpleBookDTO> books) {
        this.books = books;
    }

    public void addToBooks(SimpleBookDTO simpleBookDTO){
        this.books.add(simpleBookDTO);
    }


}
