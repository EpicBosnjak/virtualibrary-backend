package com.virtualibrary.api.dto.mapper;

import com.virtualibrary.api.dto.CategoryDTO;
import com.virtualibrary.api.dto.simple.SimpleCategoryDTO;
import com.virtualibrary.api.model.Book;
import com.virtualibrary.api.model.Category;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CategoryMapper {

    private static  String URL;

    @Value("${url.backend}")
    public void setDatabase(String url) {
        URL = url;
    }

    public static CategoryDTO createCategoryDTO(Category category){
        CategoryDTO categoryDTO= new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        Map<String,String> self = new HashMap<>();
        self.put("href",URL+"categories/" + category.getId());
        categoryDTO.addToHref("self",self);
        categoryDTO.setBooks(BookMapper.createSimpleBookDTOList(category.getBooks()));

        return categoryDTO;
    }

    public static SimpleCategoryDTO createSimpleCategoryDTO(Category category){
        SimpleCategoryDTO simpleCategoryDTO = new SimpleCategoryDTO();
        simpleCategoryDTO.setId(category.getId());
        simpleCategoryDTO.setName(category.getName());

        Map<String,String> self = new HashMap<>();
        self.put("href",URL+"categories/" + category.getId());

        simpleCategoryDTO.addToHref("self",self);

        return simpleCategoryDTO;
    }

    public static List<SimpleCategoryDTO> createSimpleCategoryDTOList(List<Category> categories){
        List<SimpleCategoryDTO> categoryDTOList = new ArrayList<>();
        for(Category category : categories){
            categoryDTOList.add(createSimpleCategoryDTO(category));
        }
        return categoryDTOList;
    }


}
