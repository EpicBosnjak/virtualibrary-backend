package com.virtualibrary.api.dto.mapper;

import com.virtualibrary.api.dto.UserDTO;
import com.virtualibrary.api.model.User;

public class UserMapper {
    public static UserDTO crateUserDTO(User user){
        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setSub(user.getSub());
        userDTO.setName(user.getFirstName() + " " +user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setRole(user.getRole().getName());
        userDTO.setBookmark(user.getBookmark());

        return userDTO;
    }
}
