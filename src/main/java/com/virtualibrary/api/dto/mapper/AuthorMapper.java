package com.virtualibrary.api.dto.mapper;

import com.virtualibrary.api.dto.AuthorDTO;
import com.virtualibrary.api.dto.simple.SimpleAuthorDTO;
import com.virtualibrary.api.model.Author;
import com.virtualibrary.api.model.Book;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AuthorMapper {
    private static  String URL;

    @Value("${url.backend}")
    public void setDatabase(String url) {
        URL = url;
    }

    public static AuthorDTO createAuthorDTO(Author author){
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setId(author.getId());
        authorDTO.setFirstName(author.getFirstName());
        authorDTO.setLastName(author.getLastName());

        authorDTO.setBooks(BookMapper.createSimpleBookDTOList(author.getBooks()));


        Map<String,String> self = new HashMap<>();
        self.put("href",URL+"authors/"+author.getId());
        authorDTO.addToLinks("self",self);

        return authorDTO;
    }

    public static SimpleAuthorDTO createSimpleAuthorDTO(Author author){
        SimpleAuthorDTO simpleAuthor = new SimpleAuthorDTO();
        simpleAuthor.setId(author.getId());
        simpleAuthor.setFirstName(author.getFirstName());
        simpleAuthor.setLastName(author.getLastName());

        Map<String,String> self = new HashMap<>();
        self.put("href",URL+"authors/"+author.getId());
        simpleAuthor.addToLinks("self",self);

        return simpleAuthor;
    }

    public static List<SimpleAuthorDTO> createSimpleAuthorDTOList(List<Author> authors){
        List<SimpleAuthorDTO> simpleAuthorDTOList = new ArrayList<>();
        for(Author author : authors){
            simpleAuthorDTOList.add(createSimpleAuthorDTO(author));
        }
        return simpleAuthorDTOList;
    }
}
