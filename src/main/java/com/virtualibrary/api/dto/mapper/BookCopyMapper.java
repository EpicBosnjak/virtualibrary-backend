package com.virtualibrary.api.dto.mapper;

import com.virtualibrary.api.dto.BookCopyDTO;
import com.virtualibrary.api.dto.simple.SimpleBookCopyDTO;
import com.virtualibrary.api.model.BookCopy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BookCopyMapper {
    private static  String URL;

    @Value("${url.backend}")
    public void setURL(String url) {
        URL = url;
    }

    public static SimpleBookCopyDTO createSimpleBookCopyDTO(BookCopy bookCopy){
        SimpleBookCopyDTO simpleBookCopy = new SimpleBookCopyDTO();
        simpleBookCopy.setId(bookCopy.getId());

        Map<String,String> self = new HashMap<>();
        self.put("href",URL+"book-copies/"+bookCopy.getId());
        simpleBookCopy.addToLink("self",self);

        Map<String,String> books = new HashMap<>();
        books.put("name",bookCopy.getBook().getName());
        books.put("href",URL+"books/" + bookCopy.getBook().getId());
        simpleBookCopy.addToLink("books",books);

        return simpleBookCopy;
    }

    public static List<SimpleBookCopyDTO> createSimpleBookCopyDTOList(List<BookCopy> bookCopies){
        List<SimpleBookCopyDTO> simpleBookCopyDTOList = new ArrayList<>();
        for(BookCopy bookCopy : bookCopies){
            simpleBookCopyDTOList.add(createSimpleBookCopyDTO(bookCopy));
        }
        return simpleBookCopyDTOList;
    }

    public static BookCopyDTO createBookCopyDTO(BookCopy bookCopy){
        BookCopyDTO bookCopyDTO = new BookCopyDTO();
        bookCopyDTO.setId(bookCopy.getId());
        bookCopyDTO.setCurrentPage(bookCopy.getCurrentPage());
        bookCopyDTO.setRentDate(bookCopy.getRentDate());
        bookCopyDTO.setExpiryDate(bookCopy.getExpiryDate());
        bookCopyDTO.setPath(bookCopy.getBook().getFileEntity().getPath());
        bookCopyDTO.setPageNumber(bookCopy.getBook().getPages());


        Map<String,String> self = new HashMap<>();
        self.put("href",URL+"book-copies/"+bookCopy.getId());
        bookCopyDTO.addToLink("self",self);

        Map<String,String> books = new HashMap<>();
        books.put("name",bookCopy.getBook().getName());
        books.put("href",URL+"books/"+bookCopy.getBook().getId());
        bookCopyDTO.addToLink("books",books);

        Map<String,String> users = new HashMap<>();
        users.put("name",bookCopy.getUser().getFirstName() + " " + bookCopy.getUser().getLastName());
        users.put("href",URL+"books/"+bookCopy.getBook().getId());
        bookCopyDTO.addToLink("users",users);

        return bookCopyDTO;
    }
}
