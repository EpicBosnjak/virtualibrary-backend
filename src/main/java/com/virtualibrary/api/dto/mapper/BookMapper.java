package com.virtualibrary.api.dto.mapper;

import com.virtualibrary.api.dto.BookDTO;
import com.virtualibrary.api.dto.simple.SimpleBookDTO;
import com.virtualibrary.api.model.Author;
import com.virtualibrary.api.model.Book;
import com.virtualibrary.api.model.Category;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BookMapper {

    private static  String URL;

    @Value("${url.backend}")
    public void setURL(String url) {
        URL = url;
    }

    public static BookDTO createBookDTO(Book book){
        BookDTO bookDTO = new BookDTO();
        bookDTO.setId(book.getId());
        bookDTO.setName(book.getName());
        bookDTO.setDescription(book.getDescription());

        bookDTO.setIsbn(book.getIsbn());
        bookDTO.setPublishedYear(book.getPublishedYear());
        bookDTO.setPages(book.getPages());

        bookDTO.addToLinks("self",selfForLinks(book));
        bookDTO.addToLinks("authors", authorsForLinks(book));
        bookDTO.addToLinks("images",imageForLinks(book));
        bookDTO.addToLinks("categories", categoriesForLinks(book));
        bookDTO.addToLinks("publisher", publisherForLinks(book));


        return bookDTO;
    }


    private static SimpleBookDTO createSimpleBookDTO(Book book) {
        SimpleBookDTO simpleBook = new SimpleBookDTO();
        simpleBook.setBookId(book.getId());
        simpleBook.setName(book.getName());

        simpleBook.addToHref("self", selfForLinks(book));
        simpleBook.addToHref("authors", authorsForLinks(book));
        simpleBook.addToHref("images",imageForLinks(book));

        return simpleBook;
    }


    public static List<SimpleBookDTO> createSimpleBookDTOList(List<Book> books){
        List<SimpleBookDTO> bookDTOList = new ArrayList<>();
        for(Book book: books){
            bookDTOList.add(createSimpleBookDTO(book));
        }
        return bookDTOList;
    }

    private static List<Map<String,String>> selfForLinks(Book book) {
        List<Map<String,String>> self = new ArrayList<>();
        Map<String, String> href = new HashMap<>();
        href.put("href",booksUrl(book.getId()));
        self.add(href);
        return self;
    }

    private static List<Map<String,String>> authorsForLinks(Book book) {
        List<Map<String, String>> authors = new ArrayList<>();
        if( book.getAuthors()==null)
            return authors;
        for (Author a : book.getAuthors()) {
            Map<String, String> author = new HashMap<>();
            author.put("name", a.getFirstName() + " " + a.getLastName());
            author.put("href", authorsUrl(a.getId()));
            authors.add(author);
        }
        return authors;
    }

    private static List<Map<String,String>> imageForLinks(Book book) {
        List<Map<String, String>> image = new ArrayList<>();
        if(book.getImage()!=null){
            Map<String,String> imageMap = new HashMap<>();
            imageMap.put("href",imageLink(book.getImage()));
            image.add(imageMap);
        }

        return image;
    }

    private static List<Map<String,String>> categoriesForLinks(Book book) {
        List<Map<String, String>> categories = new ArrayList<>();
        for (Category c : book.getCategories()) {
            Map<String, String> category = new HashMap<>();
            category.put("name", c.getName());
            category.put("href", categoriesUrl(c.getId()));
            categories.add(category);
        }
        return categories;
    }
    private static List<Map<String,String>> publisherForLinks(Book book) {
        List<Map<String,String>> publisher = new ArrayList<>();
        if(book.getPublisher()!=null){
            Map<String,String> publisherMap = new HashMap<>();
            publisherMap.put("name",book.getPublisher().getName());
            publisherMap.put("href",publishersUrl(book.getPublisher().getId()));
            publisher.add(publisherMap);
        }
        return publisher;
    }




    private static String booksUrl(int id){
        return URL+"books/" + id;
    }

    private static String authorsUrl(int id){
        return URL+"authors/"+ id;
    }

    private static String categoriesUrl(int id){
        return URL+"categories/"+ id;
    }

    private static String publishersUrl(int id){
        return URL + "publishers/" + id;
    }

    private static String imageLink(String path){
        return URL + "files/"+path;
    }

}
