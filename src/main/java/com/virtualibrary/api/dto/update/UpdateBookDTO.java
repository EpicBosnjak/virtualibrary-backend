package com.virtualibrary.api.dto.update;

public class UpdateBookDTO {
    private int id;
    private String name;
    private int publishedYear;
    private String description;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPublishedYear() {
        return publishedYear;
    }

    public String getDescription() {
        return description;
    }
}

