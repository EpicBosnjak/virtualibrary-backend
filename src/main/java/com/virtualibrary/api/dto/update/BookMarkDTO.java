package com.virtualibrary.api.dto.update;

public class BookMarkDTO {
    int userId;
    int bookMark;

    public int getUserId() {
        return userId;
    }

    public int getBookMark() {
        return bookMark;
    }
}
