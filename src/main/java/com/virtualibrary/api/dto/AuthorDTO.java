package com.virtualibrary.api.dto;


import com.virtualibrary.api.dto.simple.SimpleAuthorDTO;
import com.virtualibrary.api.dto.simple.SimpleBookDTO;

import java.util.ArrayList;
import java.util.List;

public class AuthorDTO extends SimpleAuthorDTO {
    List<SimpleBookDTO> books = new ArrayList<>();

    public List<SimpleBookDTO> getBooks() {
        return books;
    }

    public void setBooks(List<SimpleBookDTO> books) {
        this.books = books;
    }

    public void addToBooks(SimpleBookDTO book){
        this.books.add(book);
    }
}
