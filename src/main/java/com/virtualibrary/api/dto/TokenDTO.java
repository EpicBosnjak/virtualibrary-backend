package com.virtualibrary.api.dto;

import java.util.Date;

public class TokenDTO {
    String token;
    Date localtimestamp;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getLocaltimestamp() {
        return localtimestamp;
    }

    public void setLocaltimestamp(Date localtimestamp) {
        this.localtimestamp = localtimestamp;
    }
}
