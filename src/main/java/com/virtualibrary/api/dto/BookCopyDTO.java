package com.virtualibrary.api.dto;

import com.virtualibrary.api.dto.simple.SimpleBookCopyDTO;

import java.util.Date;

public class BookCopyDTO extends SimpleBookCopyDTO {
    private int currentPage;
    private int pageNumber;
    private Date rentDate;
    private Date expiryDate;
    private String path;


    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public Date getRentDate() {
        return rentDate;
    }

    public void setRentDate(Date rentDate) {
        this.rentDate = rentDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}
