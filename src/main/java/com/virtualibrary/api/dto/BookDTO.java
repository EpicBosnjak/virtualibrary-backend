package com.virtualibrary.api.dto;


import com.virtualibrary.api.model.Book;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookDTO extends Book {


    private Map<String, List<Map<String,String>>> links = new HashMap<>();

    public Map<String, List<Map<String, String>>> getLinks() {
        return links;
    }

    public void setLinks(Map<String, List<Map<String, String>>> links) {
        this.links = links;
    }

    public void addToLinks(String key, List<Map<String,String>> value){
        links.put(key, value);
    }

}
