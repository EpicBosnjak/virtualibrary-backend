package com.virtualibrary.api.dto.insert;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class InsertBookDTO implements Serializable {

    @NotEmpty(message = "Book must have ISBN")
    @Size(max = 17, message = "ISBN length must be 17 characters")
    private String isbn;

    @NotEmpty(message = "Book must have NAME.")
    private String name;

    private int publishedYear;

    @Size(max = 500, message = "Description length is over 500 characters")
    private String description;

    private List<Integer> authors;

    @NotEmpty(message = "Book must have at least ONE CATEGORY.")
    private List<Integer> categories;

    private Integer publisher;

    //Getters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPublishedYear() {
        return publishedYear;
    }

    public List<Integer> getAuthors() {
        return authors;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public Integer getPublisher() {
        return publisher;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getDescription() {
        return description;
    }

}
