package com.virtualibrary.api.dto.insert;

public class InsertTokenDTO {
    String token;

    public String getToken() {
        return token;
    }
}
