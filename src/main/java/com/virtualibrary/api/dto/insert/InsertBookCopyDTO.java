package com.virtualibrary.api.dto.insert;

public class InsertBookCopyDTO {
    private int bookId;
    private int userId;

    public int getBookId() {
        return bookId;
    }

    public int getUserId() {
        return userId;
    }

}
