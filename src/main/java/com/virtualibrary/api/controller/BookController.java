package com.virtualibrary.api.controller;

import com.virtualibrary.api.dto.BookDTO;
import com.virtualibrary.api.dto.insert.InsertBookDTO;
import com.virtualibrary.api.dto.mapper.BookMapper;
import com.virtualibrary.api.dto.simple.SimpleBookDTO;
import com.virtualibrary.api.dto.update.UpdateBookDTO;
import com.virtualibrary.api.model.Book;
import com.virtualibrary.api.model.Category;
import com.virtualibrary.api.service.BookServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("books")
public class BookController {

    private BookServiceI bookService;
    @CrossOrigin
    @PostMapping
    public ResponseEntity<BookDTO> insert (@Valid @RequestBody InsertBookDTO bookDTO){
        Book book = bookService.insert(bookDTO);
        BookDTO responseData = BookMapper.createBookDTO(book);
        return new ResponseEntity<>(responseData, HttpStatus.ACCEPTED);
    }

    @GetMapping()
    public  ResponseEntity<List<SimpleBookDTO>> getAll(){
        List<Book> books = bookService.getAll();
        if(books.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        List<SimpleBookDTO> responseData = BookMapper.createSimpleBookDTOList(books);
        return new ResponseEntity<>(responseData,HttpStatus.OK);
    }

    @GetMapping("filter/{id}")
    public  ResponseEntity<List<SimpleBookDTO>> filter(@PathVariable("id") Integer id){
        List<Book> books = bookService.getAll();
        List<Book> result = new ArrayList<>();
        for(Book book: books){
            for(Category category: book.getCategories()){
                if(category.getId() == id){
                    result.add(book);
                }
            }
        }
        if(result.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        List<SimpleBookDTO> responseData = BookMapper.createSimpleBookDTOList(result);
        return new ResponseEntity<>(responseData,HttpStatus.OK);
    }



    @GetMapping("/{id}")
    public  ResponseEntity<BookDTO> getAll(@PathVariable("id") Integer id){
        Book book = bookService.getById(id);
        BookDTO responseData = BookMapper.createBookDTO(book);
        return new ResponseEntity<>(responseData,HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping()
    public ResponseEntity<BookDTO> update(@RequestBody UpdateBookDTO book){
        Book result = bookService.update(book);
        if(result == null)
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);

        BookDTO responseData = BookMapper.createBookDTO(result);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }



    @Autowired
    public void setBookService(BookServiceI bookService) {
        this.bookService = bookService;
    }
}
