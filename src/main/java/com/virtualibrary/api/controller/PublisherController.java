package com.virtualibrary.api.controller;

import com.virtualibrary.api.dto.simple.SimplePublisherDTO;
import com.virtualibrary.api.dto.mapper.PublisherMapper;
import com.virtualibrary.api.model.Publisher;
import com.virtualibrary.api.service.PublisherServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("publishers")
public class PublisherController {

    private PublisherServiceI publisherService;

    @CrossOrigin
    @PostMapping()
    public ResponseEntity<Publisher> insert(@Valid @RequestBody Publisher publisher){
        Publisher result = publisherService.insert(publisher);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Publisher> getById(@PathVariable("id") Integer id){
        Publisher result = publisherService.getById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<SimplePublisherDTO>> getAll(){
        List<Publisher> result = publisherService.getAll();
        if (result.isEmpty())
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        List<SimplePublisherDTO> responseData = PublisherMapper.createSimplePublisherDTOList(result);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }


    @CrossOrigin
    @PutMapping()
    public ResponseEntity<Publisher> update(@RequestBody Publisher publisher){
        Publisher result = publisherService.update(publisher);
        if(result == null)
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @CrossOrigin
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Integer id){
        publisherService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @Autowired
    public void setPublisherService(PublisherServiceI publisherService) {
        this.publisherService = publisherService;
    }
}
