package com.virtualibrary.api.controller;

import com.virtualibrary.api.dto.BookCopyDTO;
import com.virtualibrary.api.dto.insert.InsertBookCopyDTO;
import com.virtualibrary.api.dto.mapper.BookCopyMapper;
import com.virtualibrary.api.dto.simple.SimpleBookCopyDTO;
import com.virtualibrary.api.model.BookCopy;
import com.virtualibrary.api.service.BookCopyServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book-copies")
public class BookCopyController {

    private BookCopyServiceI bookCopyService;

    @CrossOrigin
    @PostMapping()
    public ResponseEntity<BookCopyDTO> insert(@RequestBody InsertBookCopyDTO bookCopyDTO){
        BookCopy result = bookCopyService.insert(bookCopyDTO);
        BookCopyDTO responseData = BookCopyMapper.createBookCopyDTO(result);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<SimpleBookCopyDTO>> getForUser(@PathVariable("userId") Integer userId){
        List<BookCopy> result = bookCopyService.findByUserId(userId.intValue());
        if(result.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        List<SimpleBookCopyDTO> responseData = BookCopyMapper.createSimpleBookCopyDTOList(result);
        return new ResponseEntity<>(responseData,HttpStatus.OK);
    }

    @GetMapping("/book/{bookId}")
    public ResponseEntity<List<SimpleBookCopyDTO>> getForBook(@PathVariable("bookId") Integer bookId){
        List<BookCopy> result = bookCopyService.findByBookId(bookId.intValue());
        if(result.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        List<SimpleBookCopyDTO> responseData = BookCopyMapper.createSimpleBookCopyDTOList(result);
        return new ResponseEntity<>(responseData,HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookCopyDTO> getById(@PathVariable("id") Integer id){
        BookCopy result = bookCopyService.findById(id.intValue());
        BookCopyDTO responseData = BookCopyMapper.createBookCopyDTO(result);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping()
    public ResponseEntity<BookCopy> update (@RequestBody BookCopy bookCopy){
        BookCopy bookCopyUpdate = bookCopyService.update(bookCopy);
        return new ResponseEntity<>(bookCopyUpdate,HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/{id}/page/{page}")
    public ResponseEntity<BookCopyDTO> currentPage(@PathVariable("id") Integer id, @PathVariable("page") Integer page){
        BookCopy result = bookCopyService.currentPage(id,page);
        BookCopyDTO responseData = BookCopyMapper.createBookCopyDTO(result);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    @Autowired
    public void setBookCopyService(BookCopyServiceI bookCopyService) {
        this.bookCopyService = bookCopyService;
    }
}
