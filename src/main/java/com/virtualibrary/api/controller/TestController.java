package com.virtualibrary.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("test")
public class TestController {


    @GetMapping
    public ResponseEntity<String> test(@RequestParam("id") int id){

        return new ResponseEntity<>("Hello from Backend: " + id , HttpStatus.OK);
    }

}
