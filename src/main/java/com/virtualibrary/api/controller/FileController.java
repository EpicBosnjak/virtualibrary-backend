package com.virtualibrary.api.controller;

import com.virtualibrary.api.model.FileEntity;
import com.virtualibrary.api.service.FileServiceI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.*;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.Collections;

@RestController
@RequestMapping("files")
public class FileController {



    private FileServiceI fileService;

    private static final String PATH = "./src/main//resources/";
    private static final String EXTENSION = ".pdf";

    @CrossOrigin
    @PostMapping(value = "/{bookId}",
                 consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> fileUpload(@PathVariable("bookId") int bookId, @RequestParam("file") MultipartFile file ){
        fileService.fileUpload(file,bookId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }


    @GetMapping("/{path}/{page}")
    public ResponseEntity<Resource> getFile(@PathVariable("path")String path, @PathVariable("page") int page) throws IOException {

        Resource resource = fileService.getPage(path,page);

        if(resource== null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("image/png"))
                .body(resource);
    }

    @GetMapping("/{path}")
    public ResponseEntity<Resource> getImage(@PathVariable("path")String path){
        Resource resource = fileService.getImage(path);
        if(resource== null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("image/png"))
                .body(resource);

    }



    @Autowired
    public void setFileService(FileServiceI fileService) {
        this.fileService = fileService;
    }
}
