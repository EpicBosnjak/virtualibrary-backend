package com.virtualibrary.api.controller;

import com.virtualibrary.api.dto.LoginDTO;
import com.virtualibrary.api.dto.TokenDTO;
import com.virtualibrary.api.dto.UserDTO;
import com.virtualibrary.api.dto.insert.InsertTokenDTO;
import com.virtualibrary.api.dto.insert.InsertUserDTO;
import com.virtualibrary.api.dto.mapper.UserMapper;
import com.virtualibrary.api.model.User;
import com.virtualibrary.api.service.AuthServiceI;
import com.virtualibrary.api.service.UserServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("auth")
public class AuthController {

    private UserServiceI userService;
    private AuthServiceI authService;


    @CrossOrigin
    @PostMapping("/login")
    public ResponseEntity<UserDTO> login (@Valid @RequestBody LoginDTO loginDTO){
        UserDTO responseData = authService.login(loginDTO);
        return new ResponseEntity<>(responseData,HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/register")
    public ResponseEntity registerUser(@Valid @RequestBody InsertUserDTO user) {
        User registeredUser = authService.registerUser(user);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PostMapping("/conform-account")
    public ResponseEntity<UserDTO> confirmAccount(@RequestBody InsertTokenDTO token) {
        User result = authService.confirmUserAccount(token.getToken());
        UserDTO responseData = UserMapper.crateUserDTO(result);
        return new ResponseEntity<>(responseData,HttpStatus.OK);
    }

    @Autowired
    public void setUserService(UserServiceI userService) {
        this.userService = userService;
    }

    @Autowired
    public void setAuthService(AuthServiceI authService) {
        this.authService = authService;
    }
}
