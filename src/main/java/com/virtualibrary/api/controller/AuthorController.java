package com.virtualibrary.api.controller;

import com.virtualibrary.api.dto.AuthorDTO;
import com.virtualibrary.api.dto.simple.SimpleAuthorDTO;
import com.virtualibrary.api.dto.mapper.AuthorMapper;
import com.virtualibrary.api.model.Author;
import com.virtualibrary.api.service.AuthorServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("authors")
public class AuthorController {

    private AuthorServiceI authorService;

    @CrossOrigin
    @PostMapping()
    public ResponseEntity<Author> insert(@Valid @RequestBody Author author){
        Author result = authorService.insert(author);
        return new ResponseEntity<>(result,HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorDTO> getById(@PathVariable("id") Integer id){
        Author result = authorService.getById(id);
        AuthorDTO responseData = AuthorMapper.createAuthorDTO(result);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<SimpleAuthorDTO>> getAll(){
        List<Author> result = authorService.getAll();
        if (result.isEmpty())
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        List<SimpleAuthorDTO> responseData = AuthorMapper.createSimpleAuthorDTOList(result);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }


    @CrossOrigin
    @PutMapping()
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Author> update(@RequestBody Author author){
        Author result = authorService.update(author);
        if(result == null)
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")

    public ResponseEntity delete(@PathVariable("id") Integer id){
        authorService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Autowired
    public void setAuthorService(AuthorServiceI authorService) {
        this.authorService = authorService;
    }
}
