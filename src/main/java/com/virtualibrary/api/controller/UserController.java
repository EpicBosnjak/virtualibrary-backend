package com.virtualibrary.api.controller;

import com.virtualibrary.api.dto.update.BookMarkDTO;
import com.virtualibrary.api.model.User;
import com.virtualibrary.api.service.UserServiceI;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

@RestController
@RequestMapping("users")
public class UserController {
    private UserServiceI userService;

    @GetMapping("/{id}")
    public ResponseEntity<User> getById(@PathVariable("id") Integer id){
        User result = userService.getById(id.intValue());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping()
    public ResponseEntity<JSONObject> updateBookMark(@RequestBody BookMarkDTO bookMarkDTO){
        JSONObject  response = userService.updateBookMark(bookMarkDTO);
        return new ResponseEntity<>(response,HttpStatus.ACCEPTED);
    }

    @Autowired
    public void setUserService(UserServiceI userService) {
        this.userService = userService;
    }
}
