package com.virtualibrary.api.controller;

import com.virtualibrary.api.dto.CategoryDTO;
import com.virtualibrary.api.dto.simple.SimpleCategoryDTO;
import com.virtualibrary.api.dto.mapper.CategoryMapper;
import com.virtualibrary.api.model.Category;
import com.virtualibrary.api.service.CategoryServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("categories")
public class CategoryController {
    private CategoryServiceI categoryService;


    @CrossOrigin
    @PostMapping()
    public ResponseEntity<Category> insert(@Valid @RequestBody Category category){
        Category result = categoryService.insert(category);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CategoryDTO> getById(@PathVariable("id") Integer id){
        Category result = categoryService.getById(id);
        CategoryDTO responseData = CategoryMapper.createCategoryDTO(result);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<SimpleCategoryDTO>> getAll(){
        List<Category> result = categoryService.getAll();
        if (result.isEmpty())
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        List<SimpleCategoryDTO> responseData = CategoryMapper.createSimpleCategoryDTOList(result);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }



    @CrossOrigin
    @PutMapping()
    public ResponseEntity<Category> update(@RequestBody Category category){
        Category result = categoryService.update(category);
        if(result == null)
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Integer id){
        categoryService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @Autowired
    public void setCategoryService(CategoryServiceI categoryService) {
        this.categoryService = categoryService;
    }
}
