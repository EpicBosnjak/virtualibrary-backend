package com.virtualibrary.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Book")
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull(message = "ISBN can not be null")
    @Size(max = 17, min = 17, message = "ISBN length must be 17 characters")
    @Column(name = "isbn")
    private String isbn;

    @NotEmpty(message = "Name can not be empty")
    @NotNull(message = "Name can not be null")
    @Column(name = "name")
    private String name;

    @Size(max = 500, message = "Description length is over 500 characters")
    @Column(name = "description")
    private String description;

    @JsonIgnore
    @Column(name = "image")
    private String image;

    @Column(name = "published_year")
    private int publishedYear;

    @Column(name = "pages")
    private int pages;

    @JsonIgnore
    @Column(name = "status")
    private int status;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "Publisher_id")
    private Publisher publisher;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "File_id", referencedColumnName = "id")
    private FileEntity fileEntity;

    @JsonIgnore
    @OneToMany(mappedBy = "book")
    private List<BookCopy> bookCopes;


    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Book_Author",
            joinColumns = { @JoinColumn(name = "Book_id", referencedColumnName="id") },
            inverseJoinColumns = { @JoinColumn(name = "Author_id") }
    )
    private List<Author> authors;


    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Book_Category",
            joinColumns = { @JoinColumn(name = "Book_id",referencedColumnName="id") },
            inverseJoinColumns = { @JoinColumn(name = "Category_id") }
    )
    private List<Category> categories;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(int publishedYear) {
        this.publishedYear = publishedYear;
    }

    @JsonIgnore
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @JsonIgnore
    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    @JsonIgnore
    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public void setFileEntity(FileEntity fileEntity) {
        this.fileEntity = fileEntity;
    }

    public List<BookCopy> getBookCopes() {
        return bookCopes;
    }

    public void setBookCopes(List<BookCopy> bookCopes) {
        this.bookCopes = bookCopes;
    }

    @JsonIgnore
    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @JsonIgnore
    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
