package com.virtualibrary.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "User")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull(message = "First name can not be null")
    @NotEmpty(message = "First name cant not be empty")
    @Column(name = "first_name")
    private String firstName;

    @NotNull(message = "Last name can not be null")
    @NotEmpty(message = "Last name cant not be empty")
    @Column(name = "last_name")
    private String lastName;

    @NotNull(message = "Mail can not be null")
    @NotEmpty(message = "Mail name cant not be empty")
    @Email(message = "Mail is not valid ")
    @Column(name = "email")
    private String email;

    @Column(name = "status")
    private int status;

    @Column(name = "bookmark")
    private int bookmark;

    @Column(name = "password")
    private String password;

    @Column(name = "sub")
    private String sub;

    @OneToMany(mappedBy = "user")
    private List<BookCopy> bookCopies;

    @ManyToOne()
    @JoinColumn(name = "Role_id")
    private Role role;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<ConfirmationToken> confirmationTokens;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getBookmark() {
        return bookmark;
    }

    public void setBookmark(int bookmark) {
        this.bookmark = bookmark;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public List<BookCopy> getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(List<BookCopy> bookCopies) {
        this.bookCopies = bookCopies;
    }

    public Role getRole() {
        return role;
    }

    @JsonIgnore
    public List<ConfirmationToken> getConfirmationTokens() {
        return confirmationTokens;
    }

    public void setConfirmationTokens(List<ConfirmationToken> confirmationTokens) {
        this.confirmationTokens = confirmationTokens;
    }

    public void setRole(Role role) {
        this.role = role;
    }


}
