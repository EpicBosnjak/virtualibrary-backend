package com.virtualibrary.api.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;


@RestController
@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiError apiError = new ApiError("Bad request", HttpStatus.BAD_REQUEST.value() ,new Date());
        apiError.addDetail(ex.getLocalizedMessage());
        return new ResponseEntity<>(apiError, new HttpHeaders(),HttpStatus.BAD_REQUEST );
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiError apiError = new ApiError("Validation Failed",status.value(),new Date());
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            apiError.addDetail(error.getDefaultMessage());
        }
        return new ResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        ApiError apiError = new ApiError("Query Parameter is missing", HttpStatus.BAD_REQUEST.value() ,new Date());
        apiError.addDetail("Parameter name: " + ex.getParameterName() +" type: "+  ex.getParameterType() + "  is missing");
        return new ResponseEntity<>(apiError, new HttpHeaders(),HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(
                " method is not supported for this request. Supported methods are ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));

        ApiError apiError = new ApiError("Method not supported", HttpStatus.METHOD_NOT_ALLOWED.value() ,new Date());
        apiError.addDetail(builder.toString());

        return new ResponseEntity<>(apiError, new HttpHeaders(),HttpStatus.METHOD_NOT_ALLOWED );
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        StringBuilder builder = new StringBuilder();
        builder.append("Media type is not supported. Supported media types are: ");

        ex.getSupportedMediaTypes().forEach(t -> builder.append(t + ", "));
        String msg = builder.substring(0,builder.length()-2);

        ApiError apiError = new ApiError("Media type not supported", HttpStatus.UNSUPPORTED_MEDIA_TYPE.value() ,new Date());
        apiError.addDetail(msg);

        return new ResponseEntity<>(apiError, new HttpHeaders(),HttpStatus.UNSUPPORTED_MEDIA_TYPE );

    }






    }
