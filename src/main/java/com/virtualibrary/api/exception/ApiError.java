package com.virtualibrary.api.exception;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ApiError implements Serializable {

    private Date timestamp;
    private int status;
    private String error;
    private List<String> details;

    public ApiError(String error, int status, Date timestamp) {
        this.error = error;
        this.status = status;
        this.details = new ArrayList<>();
        this.timestamp = timestamp;
    }



    public void addDetail(String detail){
        this.details.add(detail);
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }
}
