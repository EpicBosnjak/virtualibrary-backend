package com.virtualibrary.api.repository;

import com.virtualibrary.api.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepo extends JpaRepository<Book,Integer> {
    Book findById(int Id);
    List<Book> findAllByStatus(int status);
    Book findByIsbn(String isbn);
}
