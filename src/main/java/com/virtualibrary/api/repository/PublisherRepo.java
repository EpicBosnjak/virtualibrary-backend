package com.virtualibrary.api.repository;

import com.virtualibrary.api.model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublisherRepo extends JpaRepository<Publisher,Integer> {
    Publisher findByNameIgnoreCase(String name);
    Publisher findById(int id);
}
