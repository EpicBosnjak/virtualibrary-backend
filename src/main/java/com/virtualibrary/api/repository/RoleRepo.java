package com.virtualibrary.api.repository;

import com.virtualibrary.api.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepo extends JpaRepository<Role,Integer> {
    Role findById(int id);
    Role findByName(String name);
}
