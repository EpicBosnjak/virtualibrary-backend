package com.virtualibrary.api.repository;

import com.virtualibrary.api.model.ConfirmationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfirmationTokenRepo extends JpaRepository<ConfirmationToken,Integer> {
    ConfirmationToken findByToken(String token);
}
