package com.virtualibrary.api.repository;

import com.virtualibrary.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User,Integer> {

    User findByIdAndStatus(int id, int status);

    User findByEmail(String mail);

    User findByEmailAndStatus(String email, int value);
}
