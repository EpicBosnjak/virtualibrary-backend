package com.virtualibrary.api.repository;

import com.virtualibrary.api.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepo extends JpaRepository<Category,Integer> {
    public Category findById(int id);
    public Category findByName(String name);
}
