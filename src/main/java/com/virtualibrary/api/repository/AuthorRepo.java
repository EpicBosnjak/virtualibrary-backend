package com.virtualibrary.api.repository;

import com.virtualibrary.api.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepo extends JpaRepository<Author,Integer> {
    Author findById(int id);
    Author findByFirstNameIgnoreCaseAndLastNameIgnoreCase(String firstName, String lastName);
}
