package com.virtualibrary.api.repository;

import com.virtualibrary.api.model.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepo extends JpaRepository<FileEntity,Integer> {
     FileEntity findByNameIgnoreCaseAndPath(String name, String path);

}
