package com.virtualibrary.api.repository;

import com.virtualibrary.api.model.Book;
import com.virtualibrary.api.model.BookCopy;
import com.virtualibrary.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookCopyRepo extends JpaRepository<BookCopy,Integer> {
    List<BookCopy> findAllByUser(User user);

    List<BookCopy> findAllByBook(Book book);
    BookCopy findById(int id);
}
