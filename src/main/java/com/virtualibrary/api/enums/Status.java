package com.virtualibrary.api.enums;

public enum Status {
    PADDING (0),
    ACTIVE (1),
    BANED(2),
    DELETED(3);

    private final int status;

    private Status (int status){
        this.status = status;
    }

    public int getValue(){
        return this.status;
    }
}



